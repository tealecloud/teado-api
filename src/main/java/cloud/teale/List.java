package cloud.teale;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.*;

import java.util.ArrayList;


/**
 * Root resource (exposed at "myresource" path)
 */
@Path("list")
public class List {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getIt() {
        return "Got it!";
    }

    @GET @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public String getTaskList() {
        JSONObject TaskList = new JSONObject();
        JSONArray TaskChildren = new JSONArray();

        TaskChildren.put(1);

        TaskList.put("success", true);
        TaskList.put("children", TaskChildren);

        return TaskList.toString();
    }
}
